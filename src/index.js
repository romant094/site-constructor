import { model } from './model'
import { App } from './models/app'
import './css/main.css'

new App(model).init()
