import { Text, Title, Columns, Image } from './models/blocks'

const image = 'https://static.toiimg.com/thumb/72975551.cms?width=680&height=512&imgsize=881753'
const text = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Impedit officiis praesentium provident quae quasi velit. Magnam modi officia quis voluptatum!'

export const model = [
  new Title('Pure JS site constructor', {
    tag: 'h2',
    styles: {
      background: 'linear-gradient(to left, #2500ff, #000f40)',
      color: '#fff',
      padding: '1.5rem',
      textAlign: 'center',
    },
  }),
  new Columns(
    [
      'Lorem ipsum dolor sit amet, consectetur.',
      'Lorem ipsum dolor sit amet, consectetur.',
      'Lorem ipsum dolor sit amet, consectetur.',
    ],
    {
      styles: {
        padding: '1rem',
      },
    },
  ),
  new Text(text, {
    styles: {
      background: '#eaeaf9',
      padding: '1rem',
      marginBottom: '1rem'
    },
  }),
  new Image(image, {
    styles: {
      maxWidth: '300px',
      margin: '0 auto',
    },
  }),
]
