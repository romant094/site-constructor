export const row = (
  content,
  styles = ''
) => (`
<div
  class='row'
  style="${styles}"
>
  ${content}
</div>
`)

export const column = (
  content
) => (`
<div class='col-sm'>
  ${content}
</div>
`)

export const block = type => (`
  <form name='${type}'>
    <h5>${type}</h5>
    <div class='form-group'>
      <input class='form-control form-control-sm' type='text' name='value' placeholder='value'>
    </div>
    <div class='form-group'>
      <input class='form-control form-control-sm' type='text' name='styles' placeholder='styles'>
    </div>
    <button type='submit' class='btn btn-primary btn-sm'>Submit</button>
  </form>
  <hr>
`)

export const camelToKebab = string => (
  string
    .replace(/([a-z0-9])([A-Z])/g, '$1-$2')
    .replace(/([A-Z])([A-Z])(?=[a-z])/g, '$1-$2')
    .toLowerCase()
)

export const cssTransform = styles => {
  if (typeof styles === 'string') {
    return styles
  }
  const toString = ([key, value]) => `${camelToKebab(key)}: ${value};`
  return Object
    .entries(styles)
    .reduce((acc, item) => (
      `${acc} ${toString(item)}`
    ), '')
}

String.prototype.capitalize = function() {
  return this.charAt(0).toUpperCase() + this.slice(1);
}
