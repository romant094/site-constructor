export class Site {
  constructor(selector) {
    this.$container = document.querySelector(selector)
  }

  clear() {
    while(this.$container.firstChild){
      this.$container.removeChild(this.$container.firstChild);
    }
  }

  render(model) {
    this.clear()
    model.forEach(block => {
      this.$container.insertAdjacentHTML('beforeend', block.generateHTML())
    })
  }
}
