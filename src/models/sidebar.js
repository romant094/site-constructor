import { block } from '../utils'
import * as Models from './blocks'

const mapping = {
  text: Models.Text,
}

export class Sidebar {
  constructor(selector, updateCallback) {
    this.$container = document.querySelector(selector)
    this.update = updateCallback
    this.init()
  }

  init() {
    this.$container.insertAdjacentHTML('afterbegin', this.template)
    this.$container.addEventListener('submit', this.addBlock.bind(this))
  }

  addBlock(event) {
    event.preventDefault()
    const type = event.target.name
    const value = event.target.value.value
    const styles = event.target.styles.value
    const Model = Models[type.capitalize()]
    const block = new Model(value, { styles })
    this.update(block)
    event.target.value.value = ''
    event.target.styles.value = ''
  }

  get template() {
    return [
      block('text'),
      block('title')
    ].join('')
  }
}
