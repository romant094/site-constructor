import { column, row, cssTransform } from '../utils'

class Block {
  constructor(value, options) {
    this.value = value
    this.options = options
  }

  generateHTML() {
    throw new Error('No generateHTML implemented!')
  }
}

export class Title extends Block {
  constructor(value, options) {
    super(value, options)
  }

  generateHTML() {
    const { tag = 'h1', styles = {} } = this.options
    return row(column(`<${tag}>${this.value}</${tag}>`), cssTransform(styles))
  }
}

export class Image extends Block {
  constructor(value, options) {
    super(value, options)
  }

  generateHTML() {
    const { styles = {} } = this.options
    return row(column(`<img src='${this.value}' />`), cssTransform(styles))
  }
}

export class Columns extends Block {
  constructor(value, options) {
    super(value, options)
  }

  generateHTML() {
    const { styles = {} } = this.options
    const html = this.value.map(column).join('')
    return row(html, cssTransform(styles))
  }
}

export class Text extends Block {
  constructor(value, options) {
    super(value, options)
  }

  generateHTML() {
    const { styles = {} } = this.options
    return row(column(`<p>${this.value}</p>`), cssTransform(styles))
  }
}
